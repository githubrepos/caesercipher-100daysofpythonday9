import art
print(art.logo)
alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
            'u', 'v', 'w', 'x', 'y', 'z']

#Solves the encoding and decoding, if it is not in the alphabet list it will add it just normally
def caesar(user_input, shift_value, direction_input):
    character_list = [] #Initailize the list where the sentence, letter or word will go in
    for char in user_input:  # Main loop for checking user input for the encoding/decoding                
        if char not in alphabet: # Adds it to the list when it is not in the alphabet
            character_list.append(char)
        else:            
            encrypt_letter = alphabet.index(char) # gets the number of the index
            if direction == "encode":
                encrypt_letter = encrypt_letter + shift_value #Adds the shift
                if encrypt_letter > 25: #Sees if it is over the letter index 
                    while encrypt_letter > 25: #Gets back in range, while loop for very large shift amounts
                        encrypt_letter -= 26
            else:
                encrypt_letter = encrypt_letter - shift_value
            if encrypt_letter < 0:
               while encrypt_letter < 0:
                  encrypt_letter += 26                            
            character_list.append(alphabet[encrypt_letter])   
    encrypt_word = "".join(character_list)
    print(f"The {direction_input}d text is {encrypt_word}.")

#Will perform fail safes so the user input has to be either encode or decode
while True:
  direction = input("Type 'encode' to encrypt, type 'decode' to decrypt:\n").lower()
  if direction == 'encode' or direction == 'decode':
    text = input("Type your message:\n").lower()
    shift = int(input("Type the shift number:\n"))
    break
  else:
      print("Please enter either encode or decode")

caesar(user_input=text, shift_value=shift, direction_input=direction)
#For if the user wants to run the prgoram again and will exit if they type something that isn't yes in it

run_again = input("Please type in yes to continue and anyting other than yes to exit the program.\n")
while run_again == "yes":
        while True:
            direction = input("Type 'encode' to encrypt, type 'decode' to decrypt:\n").lower()
            if direction == 'encode' or direction == 'decode':
                text = input("Type your message:\n").lower()
                shift = int(input("Type the shift number:\n"))
                break
            else:
                print("Please enter either encode or decode")
        caesar(user_input=text, shift_value=shift, direction_input=direction)
        run_again = input("Please type in yes to continue and anyting other than yes to exit the program.\n")
print("Goodbye")