#Function without Input
def greet():
  print("Hello")
  print("Neil")
  print("Robertson")
greet()

#Function with Input
def greet_with_name(name):
  print(f"Hello {name}")
  print(f"How do you do {name}?")

greet_with_name("Neil")

#Function with more than one input (parameters)

def greet_with(name, location):
  print(f"Hello {name}")
  print(f"You are located at: {location}")

greet_with("Neil", "Leeds")

#Function with keyword arguments
greet_with(location="Leeds", name="Neil")