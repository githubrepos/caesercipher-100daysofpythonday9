# CaeserCipher - 100 Days of Python: Day 8 (From Udemy Course: https://www.udemy.com/course/100-days-of-code/ by Dr. Angela Yu)

## Alternative Ways I could of done various tasks within the Cipher are the following:

- I could of use a modulo operator instead of a while loop for getting back within the index range
- When doing the run again part instead of repeating myself I could of put it all inside one while loop,
  and if it equalled an variable I initailized eariler it will do it otherwise it will end.
- Instead of appending it to a lest then rejoining it into a string, I could of just used += to put it all into a string therfore reducing a few lines of code but ending with exactly the same result. - When seeing if it is over the index range of the alphabet list I could of instead used len function on the alphabet -1 to get the last number and + 1 within the loop to get the amount I needed to either - or add by.

### Imporvements I can make: - Make it work with captial letters

- Split it into functions more to make it more easier to read

### Reflections:

Overall, I learned a lot within this project. It combined a lot of the stuff I still wasn't sure about with learning the implementation of while loops, for loops with if statements and improved by indentation. Doing it in bitsize chunks and not attempting it all at once helped with me over the 4 days I was doing it for. It proved a great boost seeing my own code being different from the lectueres proving that I got the concept down and that the soultion I came up with my own without just watching the guides with me even adding fail safe error check that the lectuer did not add.
