def prime_checker(number):
    new_number = number - 1
    while 1 < new_number:
        if number % new_number == 0:
            print("It is not a prime number.")
            break
        else:
            new_number = new_number - 1
    if new_number == 1:
        print("It is a prime number.")